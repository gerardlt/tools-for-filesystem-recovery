These tools were written to assist me in recovering data from a series of full
backups after discovering a data corruption that had been truncating files,
seemingly at random.

I needed to rebuild the directory tree so that it had the same structure as the
it had prior to recovery, but using copies from older backups where files had
been corrupted. During the period, some directories had been moved, and many
files had been added, updated or removed.

Initially, I didn't know which files were corrupted, how they had been corrupted
or for how long the corruption had been occurring. Rather than comparing live
filesystems, I wrote the dump-file-metadata script to dump out metadata from
stat along with an md5sum of the file data. I could easily store and manipulate
multiple versions of the output files on my laptop.

I wrote the compare-file-lists script to help me with the initial comparisons,
and using this I realised that all of the corrupted files had been truncated at
a 4k boundary. That pointed to a filesystem / storage problem rather than a
'rogue application'.

I later wrote the build-simulacrum script which build real directory structures
for which the files contain the file metadata. These can be easily compared and
manipulated using normal tools.

I used the output of that tool both to plan the restoration process (by doing
trial runs), and to help compare the resulting real filesystem with that
produced in the trial run, and with a the filesystem as it was just before the
recovery.

The final tally was around 3000 files having been corrupted, but only a handful
permanently lost (fortunately no serious losses).

In future, I plan to use something like dump-file-metadata on a regular basis to
help detect corruptions early.
